#include <iostream>
#include <fstream>
#include <time.h>
#include <map>
#include <vector>
#include <unistd.h>
#include <utility>

using namespace std;


using namespace std;

template <typename T> class SparseMatrix {
private:
	int rows;
	int cols;
	int cambio_fila = 0;
	int cambio_posicion = 0;
	vector<T> val;
	vector<int> colInd;
	vector<int> rowPtr;
	map<int, pair<int,int> > Rango_rowPtr; 

public:
	SparseMatrix(int r, int c) : rows(r), cols(c), rowPtr(r + 1, 1) {}


	//Obtengo el valor
	T get(int r , int c){
		
		return val[c];


	}



	void set(T valor, int r , int c) {

		//Si el valor a ingresar es != de 0
		if (valor != 0){

			//Si ya se ingresaron algunas columnas a la fila
			if(colInd.size() != 0){
				bool inserto = false;
				auto it2 = val.begin();
				//Verifico donde debo ingresarla
				for(auto it = colInd.begin() ; it != colInd.end() ; it++){

					//si esa columna no existe y es menor que la columna ingresada
					if(c < *it){
						colInd.insert(it,c);
						val.insert(it2,valor);
						inserto = true;
						break;

					//Si ya existe esa columna se suman los valores
					}else if(c == *it){

						if(*it2+valor == 0){
							val.erase(it2);
							colInd.erase(it);
						}else{
							*it2 += valor;
						}
						inserto = true;
						break;
					}
					it2++;
				}
				//Si la columna que se va a ingresar es mayor a las que estan en ColInd
				if(inserto == false){
					colInd.push_back(c);
					val.push_back(valor);
				}
			//Si no hay ninguna columna ingresada en esta fila	
			}else{
				val.push_back(valor);
				colInd.push_back(c);

			}
		}
	}


	//Set para guardar los datos leidos desde el archivo
	void set_datos(T valor, int r, int c) {

		if (valor != 0){

			if(val.size() != 0){
				bool inserto = false;
				auto it2 = val.begin();
				for(auto it = colInd.begin() ; it != colInd.end() ; it++){

					if(c < *it){
						colInd.insert(it,c);
						val.insert(it2,valor);
						inserto = true;
						break;
					}
					it2++;
				}

				if(inserto == false){
					colInd.push_back(c);
					val.push_back(valor);
				}
			}else{
				val.push_back(valor);
				colInd.push_back(c);

			}

		}
	}


	//Multiplicacion de la Sparse
	SparseMatrix<T> mult(SparseMatrix<T> &b) {

		//Vector que guardara los datos de las filas resultantes
		vector<SparseMatrix<int> > V2(rows,{1,cols});

		//Cantidad de filas con valores
		for(int i = 0 ; i < rowPtr.size()-1 ; i++){ 

			//valores de la fila 
			for(int j = this->rowPtr[i] ; j < this->rowPtr[i+1] ; j++){
				
				//obtengo la fila que debo multiplicar (Rango del rowptr)
				int desde, hasta;
				getRango_RowPtr(b.getCol(j), desde, hasta);


				for(int k = desde ; k < hasta ; k++){

					T mult = this->get(i,j)*b.get(j,k);

					//Guardo en la (fila , columna de la segunda fila visitada)
					V2[i].set(mult, i ,getCol(k));

				}

			}

		}

		//reviso las filas que se pudieron hacer 0
		int contador = 0;
		for(int i = 0 ; i < V2.size() ; i++){

			if(V2[i].val.size() != 0){
				contador++;
			}

		}
		SparseMatrix<T> r(contador,contador);
		//Agrego todos los datos a una Sparse Matrix
		r.agregar(V2);

		return r;
	}


	//Obtengo el valor de la columna
	T getCol(T k) { 

		return colInd[k]; 

	}

	//Imprimo el rango que le pertenece a cada fila
	void imprimir_rango(){

		for (auto it = Rango_rowPtr.begin() ; it != Rango_rowPtr.end() ; it++){

			cout << "Fila: " << it->first << " Desde: " << it->second.first << " Hasta: " << it->second.second << endl;
		}


	}

	//Obtengo el rango segun la fila
	void getRango_RowPtr(int columna , int &i , int &j){


		i = Rango_rowPtr[columna].first;
		j = Rango_rowPtr[columna].second;

	}

	//Imprimo los valores de val
	void imprimir_val() {
		for (int i = 0; i < val.size(); i++) {
			cout << val[i] << " ";
		}
		cout << endl;
	}

	//Imprimo los valores de colInd
	void imprimier_colInd() {
		for (int i = 0; i < colInd.size(); i++) {
			cout << colInd[i] << " ";
		}
		cout << endl;
	}

	//Imprimo los valores de rowPtr
	void imprimir_rowPtr() {

		for (int i = 0; i < rowPtr.size(); i++) {
			cout << rowPtr[i] << " ";
		}
		cout << endl;
	}

	//Obtengo el tamaño de val
	int val_size(){

		return val.size();

	}

	//Obtengo el tamaño de colInd
	int col_size(){

		return colInd.size();

	}

	//Obtengo el tamaño de rowPtr
	int ptr_size(){

		return rowPtr.size();

	}

	//Agrego los valores de las Sparse resultante a un sola Sparse
	void agregar(vector<SparseMatrix<T>> &V){


		int contador = 0;
		for(int i = 0 ; i < V.size() ; i++){ 

			if(V[i].val.size() != 0){
				this->rowPtr[contador] = this->val.size();
				int x = this->val.size();

				for(int j = 0 ; j < V[i].val.size(); j++){
					this->val.push_back(V[i].val[j]);
				}

				for(int j = 0 ; j < V[i].val.size(); j++){
					this->colInd.push_back(V[i].colInd[j]); 
				}
				contador++;

				Rango_rowPtr[i] = make_pair(x,this->val.size());

			}
		}
		this->rowPtr[contador++] = this->val.size();

	}


};


int main (int argc, char** argv) {


	if ( argc > 2 ) {

		cout << "Error, cantidad de parametros no valida" << endl;
		return -1;

	}


	if (argv[1] == NULL) {

		cout << "Error, cantidad de parametros no valida. Falta archivo" << endl;
		return -1;

	}

	ifstream datos(argv[1]);

	int contador = 0;
	string hola;
	int filas, arcos;

	//Leo las lineas antes de los nodos
	while(contador <= 7){


		if(contador == 4){

			datos >> hola;
			datos >> hola;
			datos >> hola;
			filas = atoi(hola.c_str());
			datos >> hola;
			arcos = atoi(hola.c_str());	

		}else{

			getline(datos, hola);
		}

		contador++;
	}


	vector<SparseMatrix<int> > V(filas-1,{1,filas});

	string numero;
	string letra;
	int fila1;
	int columna1;
	int valor;

	string basura;
	contador = 0;

	//Leo todos los arcos para ser ingresados en la Sparse Matrix
	while (contador != arcos) {

		datos >> letra;
		datos >> numero;
		fila1 = atoi(numero.c_str());
		datos >> numero;
		columna1 = atoi(numero.c_str());
		datos >> numero;
		valor = atoi(numero.c_str());

		V[fila1-1].set_datos(valor, fila1-1, columna1-1);
		contador++;

	}
	datos.close();

	contador = 0;
	for(int i = 0 ; i < V.size() ; i++){ 

		if(V[i].val_size() != 0){
			contador++;
		}
	}

	SparseMatrix<int> resultante2(contador,contador);

	resultante2.agregar(V);

	SparseMatrix<int> final(contador,contador);
	//resultante2.imprimir_val();
	//resultante2.imprimier_colInd();
	//resultante2.imprimir_rowPtr();

	clock_t t;
	t = clock();
	cout << "comenze a multiplicar" << endl;

	//Comienzo a multiplicar
	final = resultante2.mult(resultante2);

	//final.imprimir_val();
	//final.imprimier_colInd();
	//final.imprimir_rowPtr();


	t = clock() - t;
	printf ("It took me %d clicks (%f seconds).\n",(int)t,((float)t)/CLOCKS_PER_SEC);
	cout << "Termine de multiplicar" << endl;


}
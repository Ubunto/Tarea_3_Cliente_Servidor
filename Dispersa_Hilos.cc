#include <string>
#include <iostream>
#include <math.h>
#include <cstdlib>
#include <thread>
#include <queue>
#include <exception>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <time.h>
#include <fstream>
#include <map>
#include <utility>
#include <atomic>


using namespace std;

//Una cola segura para asegurar que los hilos no colisionen
template<typename T> class threadsafe_queue{

private:

	mutable mutex mut;
	queue<T> data_queue;
	condition_variable data_cond;

public:

	threadsafe_queue(){}

	void push(T data){

		lock_guard<mutex> lk(mut);
		data_queue.push(move(data));
    	data_cond.notify_one(); // cuando pusheo notifico al wait
    }

    void wait_and_pop(T& value){

    	unique_lock<mutex> lk(mut);
    data_cond.wait(lk,[this]{return !data_queue.empty();}); //Espero
    value = move(data_queue.front());
    data_queue.pop();
}

bool try_pop(T& value){

	lock_guard<mutex> lk(mut);
	if(data_queue.empty())
		return false;
	value = move(data_queue.front());
	data_queue.pop();
	return true;
}

bool empty() const{

	lock_guard<mutex> lk(mut);
	return data_queue.empty();
}

};


//ThreadPool para manejar que mis hilos terminen bien
class ThreadPool {
public:
	ThreadPool() : running_(true) {
		unsigned int thread_count = std::thread::hardware_concurrency();
		try {
			for (unsigned int i = 0; i < thread_count; i++) {
				threads_.emplace_back(&ThreadPool::WorkerThread, this);
			}
		} catch (...) {
			running_ = false;
			throw;
		}
	}

	~ThreadPool() {
		running_ = false;
		for (auto& thread : threads_) {
			if (thread.joinable()) thread.join();
		}
	}

	std::vector<std::thread::id> GetThreadIds() const {
		std::vector<std::thread::id> ids;
		for (auto& thread : threads_) {
			ids.emplace_back(thread.get_id());
		}
		return ids;
	}

    template <typename FunctionType>
	void Submit(FunctionType f) {
		work_queue.push(f);
	}

private:
	void WorkerThread() {
		while (running_ || !work_queue.empty()) {
			std::function<void()> task;
			if (work_queue.try_pop(task)) {
				task();
			} else {
				std::this_thread::yield();
			}
		}
	}

private:
	std::atomic_bool running_;
	std::vector<std::thread> threads_;
	threadsafe_queue<std::function<void()> > work_queue;
};



template <typename T> class SparseMatrix {
private:
	int rows;
	int cols;
	int cambio_fila = 0;
	int cambio_posicion = 0;
	vector<T> val;
	vector<int> colInd;
	vector<int> rowPtr;
	map<int, pair<int,int> > Rango_rowPtr;

public:
	SparseMatrix(int r, int c) : rows(r), cols(c), rowPtr(r + 1, 1) {}


	//Obtengo el valor
	T get(int r , int c){
		
		return val[c];


	}



	void set(T valor, int r , int c) {

		//Si el valor a ingresar es != de 0
		if (valor != 0){

			//Si ya se ingresaron algunas columnas a la fila
			if(colInd.size() != 0){
				bool inserto = false;
				auto it2 = val.begin();
				//Verifico donde debo ingresarla
				for(auto it = colInd.begin() ; it != colInd.end() ; it++){

					//si esa columna no existe y es menor que la columna ingresada
					if(c < *it){
						colInd.insert(it,c);
						val.insert(it2,valor);
						inserto = true;
						break;

					//Si ya existe esa columna se suman los valores
					}else if(c == *it){

						if(*it2+valor == 0){
							val.erase(it2);
							colInd.erase(it);
						}else{
							*it2 += valor;
						}
						inserto = true;
						break;
					}
					it2++;
				}
				//Si la columna que se va a ingresar es mayor a las que estan en ColInd
				if(inserto == false){
					colInd.push_back(c);
					val.push_back(valor);
				}
			//Si no hay ninguna columna ingresada en esta fila	
			}else{
				val.push_back(valor);
				colInd.push_back(c);

			}
		}
	}


	//Set para guardar los datos leidos desde el archivo
	void set_datos(T valor, int r, int c) {

		if (valor != 0){

			if(val.size() != 0){
				bool inserto = false;
				auto it2 = val.begin();
				for(auto it = colInd.begin() ; it != colInd.end() ; it++){

					if(c < *it){
						colInd.insert(it,c);
						val.insert(it2,valor);
						inserto = true;
						break;
					}
					it2++;
				}

				if(inserto == false){
					colInd.push_back(c);
					val.push_back(valor);
				}
			}else{
				val.push_back(valor);
				colInd.push_back(c);

			}

		}
	}

	//Multiplicacion de la Sparse
	SparseMatrix<T> mult(SparseMatrix<T> &b) {
		
		//Vector que guardara los datos de las filas resultantes
		vector<SparseMatrix<int> > V2(rows,{1,cols});

		//Función Lambda
		auto multiplicacion = [&](int l) -> void {

			//valores de la fila 
			for(int j = this->rowPtr[l] ; j < this->rowPtr[l+1] ; j++){

				//obtengo la fila que debo multiplicar (Rango del rowptr)		
				int desde, hasta;
				getRango_RowPtr(b.getCol(j), desde, hasta);

				for(int k = desde ; k < hasta ; k++){

					T mult = this->get(l,j)*b.get(j,k);
					
					//Guardo en la (fila , columna de la segunda fila visitada)
					V2[l].set(mult, l ,getCol(k));

				}

			}

		};

		//Creo el threadpool
		ThreadPool* pool = new ThreadPool();

		//Cantidad de filas con valores
		for (int i = 0; i < rowPtr.size() - 1; i++) {

			pool->Submit([&multiplicacion, i] { multiplicacion(i); });

		}
		//espera hasta que terminen todos los hilos
		delete pool;

		//reviso las filas que se pudieron hacer 0
		int contador = 0;
		for(int i = 0 ; i < V2.size() ; i++){

			if(V2[i].val.size() != 0){
				contador++;
			}

		}

		SparseMatrix<T> r(contador,contador);
		//Agrego todos los datos a una Sparse Matrix
		r.agregar(V2);

		return r;
	}

	//Obtengo el valor de la columna
	T getCol(T k) { 

		return colInd[k]; 

	}

	//Imprimo el rango que le pertenece a cada fila
	void imprimir_rango(){

		for (auto it = Rango_rowPtr.begin() ; it != Rango_rowPtr.end() ; it++){

			cout << "Fila: " << it->first << " Desde: " << it->second.first << " Hasta: " << it->second.second << endl;
		}


	}

	//Obtengo el rango segun la fila
	void getRango_RowPtr(int columna , int &i , int &j){


		i = Rango_rowPtr[columna].first;
		j = Rango_rowPtr[columna].second;

	}

	//Imprimo los valores de val
	void imprimir_val() {
		for (int i = 0; i < val.size(); i++) {
			cout << val[i] << " ";
		}
		cout << endl;
	}

	//Imprimo los valores de colInd
	void imprimier_colInd() {
		for (int i = 0; i < colInd.size(); i++) {
			cout << colInd[i] << " ";
		}
		cout << endl;
	}

	//Imprimo los valores de rowPtr
	void imprimir_rowPtr() {

		for (int i = 0; i < rowPtr.size(); i++) {
			cout << rowPtr[i] << " ";
		}
		cout << endl;
	}

	//Obtengo el tamaño de val
	int val_size(){

		return val.size();

	}

	//Obtengo el tamaño de colInd
	int col_size(){

		return colInd.size();

	}

	//Obtengo el tamaño de rowPtr
	int ptr_size(){

		return rowPtr.size();

	}

	//Agrego los valores de las Sparse resultante a un sola Sparse
	void agregar(vector<SparseMatrix<T>> &V){


		int contador = 0;
		for(int i = 0 ; i < V.size() ; i++){ 

			if(V[i].val.size() != 0){
				this->rowPtr[contador] = this->val.size();
				int x = this->val.size();

				for(int j = 0 ; j < V[i].val.size(); j++){
					this->val.push_back(V[i].val[j]);
				}

				for(int j = 0 ; j < V[i].val.size(); j++){
					this->colInd.push_back(V[i].colInd[j]); 
				}
				contador++;

				Rango_rowPtr[i] = make_pair(x,this->val.size());

			}
		}
		this->rowPtr[contador++] = this->val.size();

	}


};





int main (int argc, char** argv) {


	if ( argc > 2 ) {

		cout << "Error, cantidad de parametros no valida" << endl;
		return -1;

	}


	if (argv[1] == NULL) {

		cout << "Error, cantidad de parametros no valida. Falta archivo" << endl;
		return -1;

	}

	ifstream datos(argv[1]);

	int contador = 0;
	string hola;
	int filas, arcos;

	//Leo las lineas antes de los nodos
	while(contador <= 7){


		if(contador == 4){

			datos >> hola;
			datos >> hola;
			datos >> hola;
			filas = atoi(hola.c_str());
			datos >> hola;
			arcos = atoi(hola.c_str());	

		}else{

			getline(datos, hola);
		}

		contador++;
	}


	vector<SparseMatrix<int> > V(filas-1,{1,filas});

	string numero;
	string letra;
	int fila1;
	int columna1;
	int valor;

	string basura;
	contador = 0;

	//Leo todos los arcos para ser ingresados en la Sparse Matrix
	while (contador != arcos) {
		datos >> letra;
		datos >> numero;
		fila1 = atoi(numero.c_str());
		datos >> numero;
		columna1 = atoi(numero.c_str());
		datos >> numero;
		valor = atoi(numero.c_str());

		V[fila1-1].set_datos(valor, fila1-1, columna1-1);
		contador++;

	}
	datos.close();

	contador = 0;
	for(int i = 0 ; i < V.size() ; i++){ 

		if(V[i].val_size() != 0){
			contador++;
		}
	}

	SparseMatrix<int> resultante2(contador,contador);

	resultante2.agregar(V);

	SparseMatrix<int> final(contador,contador);
	//resultante2.imprimir_val();
	//resultante2.imprimier_colInd();
	//resultante2.imprimir_rowPtr();
	int n = std::thread::hardware_concurrency();
	clock_t t;
	t = clock();
	cout << "comenze a multiplicar" << endl;
	final = resultante2.mult(resultante2);

	//final.imprimir_val();
	//final.imprimier_colInd();
	//final.imprimir_rowPtr();

	t = clock() - t;

	printf ("It took me %d clicks (%f seconds).\n",(int)(t/n),((((float)t)/CLOCKS_PER_SEC))/n);
	cout << "Termine de multiplicar" << endl;


	return 0;
}
